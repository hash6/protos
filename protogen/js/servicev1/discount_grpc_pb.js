// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var servicev1_discount_pb = require('../servicev1/discount_pb.js');
var servicev1_product_pb = require('../servicev1/product_pb.js');
var servicev1_user_pb = require('../servicev1/user_pb.js');

function serialize_service_v1_DiscountProductRequest(arg) {
  if (!(arg instanceof servicev1_discount_pb.DiscountProductRequest)) {
    throw new Error('Expected argument of type service.v1.DiscountProductRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_service_v1_DiscountProductRequest(buffer_arg) {
  return servicev1_discount_pb.DiscountProductRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_service_v1_DiscountProductResponse(arg) {
  if (!(arg instanceof servicev1_discount_pb.DiscountProductResponse)) {
    throw new Error('Expected argument of type service.v1.DiscountProductResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_service_v1_DiscountProductResponse(buffer_arg) {
  return servicev1_discount_pb.DiscountProductResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var DiscountProductService = exports.DiscountProductService = {
  discountProduct: {
    path: '/service.v1.DiscountProduct/DiscountProduct',
    requestStream: false,
    responseStream: false,
    requestType: servicev1_discount_pb.DiscountProductRequest,
    responseType: servicev1_discount_pb.DiscountProductResponse,
    requestSerialize: serialize_service_v1_DiscountProductRequest,
    requestDeserialize: deserialize_service_v1_DiscountProductRequest,
    responseSerialize: serialize_service_v1_DiscountProductResponse,
    responseDeserialize: deserialize_service_v1_DiscountProductResponse,
  },
};

exports.DiscountProductClient = grpc.makeGenericClientConstructor(DiscountProductService);
