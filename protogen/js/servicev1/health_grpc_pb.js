// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var servicev1_health_pb = require('../servicev1/health_pb.js');

function serialize_service_v1_GetPingMessage(arg) {
  if (!(arg instanceof servicev1_health_pb.GetPingMessage)) {
    throw new Error('Expected argument of type service.v1.GetPingMessage');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_service_v1_GetPingMessage(buffer_arg) {
  return servicev1_health_pb.GetPingMessage.deserializeBinary(new Uint8Array(buffer_arg));
}


// Service get ping.
var PingService = exports.PingService = {
  // To check the service is up.
getPing: {
    path: '/service.v1.Ping/GetPing',
    requestStream: false,
    responseStream: false,
    requestType: servicev1_health_pb.GetPingMessage,
    responseType: servicev1_health_pb.GetPingMessage,
    requestSerialize: serialize_service_v1_GetPingMessage,
    requestDeserialize: deserialize_service_v1_GetPingMessage,
    responseSerialize: serialize_service_v1_GetPingMessage,
    responseDeserialize: deserialize_service_v1_GetPingMessage,
  },
};

exports.PingClient = grpc.makeGenericClientConstructor(PingService);
