module gitlab.com/hash6/protos

go 1.15

require (
	github.com/golang/protobuf v1.4.3
	github.com/uber/prototool v1.10.0
	google.golang.org/grpc v1.34.0
	google.golang.org/protobuf v1.25.0
)
