GO ?= go
GOGETV ?= $(GO) get -v

GIT ?= git
GITDIFF ?= $(GIT) diff

.PHONY: generate
generate: install
	rm -rf ./protogen
	PATH="$$PWD/bin:$$PATH" prototool generate

.PHONY: install
install: export GO111MODULE := on
install: export GOBIN := $(PWD)/bin
install:
	$(GOGETV) github.com/uber/prototool/cmd/prototool
	$(GOGETV) github.com/golang/protobuf/protoc-gen-go

.PHONY: lint
lint: install
	PATH="$$PWD/bin:$$PATH" prototool lint

.PHONY: clean
clean:
	@echo "cleaning ... "
	rm -rf ./bin/

.PHONY: git/diff
git/diff:
	@if ! $(GITDIFF) --quiet; then \
		printf 'Found changes on local workspace. Please run this target and commit the changes\n' ; \
		exit 1; \
	fi

